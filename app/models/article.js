module.exports = function(sequelize, DataTypes) {
    var Article = sequelize.define("Article", {
        title: DataTypes.STRING,
        content: DataTypes.TEXT
    });

    //create the table
    Article.sync();

    return Article;
};
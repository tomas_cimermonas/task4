module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define("User", {
        name: DataTypes.STRING,
        password: DataTypes.TEXT,
        admin: DataTypes.BOOLEAN
    });

    //create the table
    User.sync();

    return User;
};
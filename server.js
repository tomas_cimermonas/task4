//dependencies
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const Article = require('./app/models/article');
const User = require('./app/models/user');
const Sequelize = require('sequelize');
const config = require('./config/config');
var jwt = require('jsonwebtoken'); //  create, sign, and verify token

// //database setup
const sequelize = new Sequelize(config.db_table, config.db_user, config.db_pass, {
	dialect: config.db_dialect,
	logging: config.db_logging
});

module.exports = {
	databaseConnection: function() {
		sequelize
			.authenticate()
			.then(() => {
				return 1;
			})
			.catch((error) => {
				return error;
			});
	}
};

//get data from post
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//log requests
app.use(morgan('dev'));
// set the static files location 
app.use(express.static(`${__dirname}/public`));

const port = process.env.port || 8000;

//routes
const router = express.Router();
const unsecureRouter = express.Router();


//initialize models
const article = new Article(sequelize, Sequelize);
const user = new User(sequelize, Sequelize);

// middleware to use for all requests
router.use((req, res, next) => {
	// check header or url parameters or post parameters for token
	var token = req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
		jwt.verify(token, config.secret, (error, decoded) => {
			if (error) {
				return res.json({
					success: false,
					message: 'Failed to verify token'
				});
			}
			req.decoded = decoded;
			next();
		});
	} else {
		return res.json({
			success: false,
			message: 'No token found'
		});
	}
});

//User API -----------------------------------------
unsecureRouter.route('/user/create')
	/*
	 *TODO
	 *Password hashing
	 *Custom users
	 */
	.post((req, res) => {
		user.create({
			name: req.body.name,
			password: req.body.pass,
			admin: true
		}).then(() => {
			res.json({
				success: true,
				message: 'created'
			});
		});
	});

unsecureRouter.route('/login')
	.post((req, res) => {
		user.find({
			where: {
				name: req.body.name
			}
		}).then((authUser) => {
			if (authUser.password != req.body.pass) {
				res.json({ message: 'invalid credentials' });
			} else {
				//new object to assign payload
				const payload = { username: authUser.name, password: authUser.pass };
				// create a token
				let token = jwt.sign(payload, config.secret, {
					expiresIn: 1440 // expires in 1 hour
				});
				res.json({
					success: true,
					message: 'logged in',
					token: token
				});
			}
		});
	});

//Article API -----------------------------------------
//public
//routing for /articles
unsecureRouter.route('/articles')
	//create a new article (/articles)
	.post((req, res) => {
		article.create({
			title: req.body.title,
			content: req.body.content
		}).then(() => {
			res.json({
				message: 'posted',
				success: true
			});
		}).catch((error) => {
			res.json({
				error: error,
				success: false
			});
		});
	})

	//get all articles (/articles)
	.get((req, res) => {
		article.findAll().then((articles) => {
			res.json({
				articles: articles
			});
		}).catch((error) => {
			res.json({
				error: error,
				success: false
			});
		});
	});

//private
//routing for specific article (/articles/:article_id)
router.route('/articles/:article_id')
	//get object by id (/articles/:article_id)
	.get((req, res) => {
		article.findById(req.params.article_id).then((articleObj) => {
			res.json(articleObj);
		}).catch((error) => {
			res.json({
				error: error,
				success: false
			});
		});
	})

//update object (/articles/:article_id)
	.put((req, res) => {
		article.findById(req.params.article_id).then((articleObj) => {
			articleObj.updateAttributes({
				title: req.body.title,
				content: req.body.content
			}).then(() => {
				res.json({
					success: true,
					message: 'updated'
				});
			});
		}).catch((error) => {
			res.json({
				success: false,
				error: error
			});
		});
	})

//delete object by id
	.delete((req, res) => {
		article.findById(req.params.article_id).then((articleObj) => {
			articleObj.destroy().then(() => {
				res.json({
					success: true,
					message: 'deleted'
				});
			});
		}).catch((error) => {
			res.json({
				success: false,
				error: error
			});
		});
	});

//test route
router.get('*', (req, res) => {
	res.sendfile('/public/index.html');
});

//routes registering
app.use('/api', router);
app.use('/', unsecureRouter);

//start the server
app.listen(port);
// expose app
exports = module.exports = app;

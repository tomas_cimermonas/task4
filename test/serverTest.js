const request = require('supertest'),
	app = require('../server');

describe('Server testing: ', () => {
	it('It is reachable', (done) => {
		request(app).get('/')
			.expect(200, done);
	});

	it('shows articles', (done) => {
		request(app).get('/articles')
			.expect(200)
			.expect(/articles/, done);
	});

	it('has least 1 article', (done) => {
		request(app).get('/articles')
			.expect(200)
			.expect(/title/, done);
	});

	it(' allows to create new users', (done) => {
		let newUser = {
			name: 'tesUser',
			password: 'unsecurePassword',
			admin: false
		};
		request(app).post('/user/create')
			.send(newUser)
			.expect(200)
			.expect({
				success: true,
				message: 'created'
			}, done);
	});
});

module.exports = {
    'secret': 'supersecretword',
    'db_user': 'root',
    'db_pass': '',
    'db_table': 'task_database',
    'db_dialect': 'mysql',
    'db_logging': false
};